const express = require("express");
const bp = require("body-parser");
const serverless = require("serverless-http");
const cors = require("cors");

const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport(
  {
    host: "smtp.yandex.ru",
    port: 465,
    secure: true,
    auth: {
      user: "info@agropro52.ru",
      pass: "agropro-mail-pass",
    },
    tls: { rejectUnauthorized: false },
  },
  {
    from: `process.env.EMAIL <info@agropro52.ru>`,
  }
);

const getGailOptions = (name, phone, comment, isLocal) => {
  return {
    from: "info@agropro52.ru",
    to: "info@agropro52.ru",
    subject: "Письмо отправлен с помощью NodeJS",
    text: `Имя: ${name}, телефон: ${phone}, коментарий: ${
      comment ?? comment
    }. ${new Date().toString()}, is localc ${isLocal}`,
  };
};
class Mailer {
  async sendMail(req, res) {
    const { name, phone, comment, isLocal } = req.body;
    try {
      await transporter.sendMail(
        getGailOptions(name, phone, comment, isLocal),
        function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent successfully", info);
          }
          transporter.close();
        }
      );
      res.json({
        from: "info@agropro52.ru",
        to: "info@agropro52.ru",
        subject: "Письмо отправлен с помощью NodeJS",
        isLocal,
      });
    } catch (error) {
      throw new Error(error);
    }
  }
}

const mailer = new Mailer();

const app = express();
const router = express.Router();

app.use(cors());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();

  app.options("*", (req, res) => {
    res.header(
      "Access-Control-Allow-Methods",
      "GET, PATCH, PUT, POST, DELETE, OPTIONS"
    );
    res.send();
  });
});

app.use(bp.json());

router.get("/test", (req, res) => {
  res.json({ test: "test" });
});

router.post("/sendMail", mailer.sendMail);

app.listen(9000, () => {
  console.log(`Listening at http://localhost:${9000}`);
});

app.use(router);

module.exports.handler = serverless(app);
